<!DOCTYPE html>

<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for Linux (vers 25 March 2009), see www.w3.org">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>STP Reference</title>
  <link rel="stylesheet" href=
  "https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"
  type="text/css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel=
  "stylesheet" type="text/css">
  <link rel="stylesheet" href="style.css" type="text/css">
</head>

<body>
  <nav>
    <div class="nav-wrapper blue-grey lighten-2">
      <div class="black-text">
        <h3 class="flat-heading brand-logo center">Ethernet Bridge Spanning
        Tree Protocol</h3>
      </div>
    </div>
  </nav>

  <div class="container">
    <h2>Background</h2>

    <div class="row">
      <div class="col s12">
        <ul class="collapsible" data-collapsible="expandable">
          <li>
            <div class="collapsible-header">
              <i class="material-icons">settings_ethernet</i>Ethernet
            </div>

            <div class="collapsible-body">
              <span>Ethernet is a physical communication medium used by devices
              all over the world to send data and connect to the Internet. The
              smallest unit of connectivity in Ethernet is called a Local Area
              Network (LAN). A LAN is a set of Ethernet wires and/or
              hubs/repeaters which are directly connected. In theory any number
              of hosts can be connected on the same LAN.</span>
            </div>
          </li>

          <li>
            <div class="collapsible-header">
              <i class="material-icons">device_hub</i>Broadcast
            </div>

            <div class="collapsible-body">
              <span>Ethernet is a broadcast medium. This means while one host
              on a LAN is sending a signal (packet), no other host can send on
              that LAN or both signals will be corrupted. When this occurs
              it&#39;s called a collision. The Ethernet protocol provides a way
              for devices to cooperatively share the medium and detect/recover
              from collisions, but the more devices there are on a single LAN
              the more difficult it is to efficiently share the medium between
              them. In order to effectively scale the number of devices in the
              network it is necessary to connect multiple LANs together using
              Bridges.</span>
            </div>
          </li>

          <li>
            <div class="collapsible-header">
              <i class="material-icons">transform</i>Bridges
            </div>

            <div class="collapsible-body">
              <span>Bridges are used to connect multiple LANs together in order
              to create smaller groups of hosts within which collisions can
              occur, called collision domains. When a host on one LAN sends a
              signal to a host on a different LAN connected by a bridge, the
              bridge forwards that packet onto the other LAN for the host to
              receive. In order to efficiently forward packets a bridge must
              learn the locations of end hosts. A bridge learns these locations
              from packets sent by those end hosts.</span>
            </div>
          </li>
        </ul>
      </div>
    </div>

    <h2>STP</h2>

    <div class="row">
      <div class="col s12">
        <ul class="collapsible" data-collapsible="expandable">
          <li>
            <div class="collapsible-header active">
              <i class="material-icons">sync_disabled</i>Why?
            </div>

            <div class="collapsible-body">
              <div>
                <h3>Packet Forwarding</h3>To minimize packet redundancy bridges
                learn the locations of hosts by listening to packets passing
                through and recording the receiving port and the sending host
                to record that the sending host can be reached via the
                receiving port. This data needs to timeout after a certain time
                in case a host moves to a different place in the network, and
                if the bridge becomes aware of changes in the network topology
                the forwarding table must be cleared.
              </div>

              <div>
                <h3>Why is STP necessary?</h3>The Ethernet Bridge Spanning Tree
                Protocol (STP) is a distributed algorithm to prevent loops in
                the network. A loop occurs when a packet takes a path through
                the network which causes it to be retransmitted multiple times
                by the same bridge, such as below:<br>
                <br>

                <svg id="network-diagram" width="300" height="300">
                  <line x1="150" y1="50" x2="50" y2="150" style=
                  "stroke:black;stroke-width:2;"></line>

                  <line x1="150" y1="50" x2="250" y2="150" style=
                  "stroke:black;stroke-width:2;"></line>

                  <line x1="150" y1="250" x2="50" y2="150" style=
                  "stroke:black;stroke-width:2;"></line>

                  <line x1="150" y1="250" x2="250" y2="150" style=
                  "stroke:black;stroke-width:2;"></line>

                  <circle cx="150" cy="50" r="20" stroke="black" stroke-width=
                  "1" fill="green"></circle>

                  <text x="142" y="55" fill="white">HA</text>

                  <circle cx="50" cy="150" r="25" stroke="black" stroke-width=
                  "1" fill="blue"></circle>

                  <text x="43" y="155" fill="white">B1</text>

                  <circle cx="150" cy="250" r="20" stroke="black" stroke-width=
                  "1" fill="green"></circle>

                  <text x="142" y="255" fill="white">HB</text>

                  <circle cx="250" cy="150" r="25" stroke="black" stroke-width=
                  "1" fill="blue"></circle>

                  <text x="243" y="157" fill="white">B2</text>
                </svg><br>
                Consider the network above and the following sequence of
                events:

                <ol>
                  <li>Host A sends a packet to Host B</li>

                  <li>Bridges 1 and 2 receive the packet on the upper port</li>

                  <li>Both bridges forward the packet on to the upper port</li>

                  <li>Both bridges receive the packet on the lower port</li>

                  <li>Both bridge forward the packet on to the upper port</li>
                </ol>A cycle has happened, in order to avoid cycles or loops in
                the network, the bridges must form a spanning tree over the
                network. STP is a way to build that tree without any bridge
                knowing the state of the whole network.
              </div>
            </div>
          </li>

          <li>
            <div class="collapsible-header">
              <i class="material-icons">share</i>What?
            </div>

            <div class="collapsible-body">
              <h3>Goal</h3><span>The goal of STP is to build a spanning tree in
              the network by having each bridge disable a subset of its ports
              such that no cycles exist in the network graph. In order to know
              which ports to disable the bridges exchange information about the
              state of the network with their direct neighbors. The information
              a bridge needs about the network to make correct forwarding
              decisions is as follows:</span>

              <ol>
                <li><span>Which bridge is at the root of the tree</span></li>

                <li><span>Which port leads toward the root with minimal cost in
                hops</span></li>

                <li><span>The cost of the path to the root for bridges on LANs
                connected to ports on the bridge</span></li>
              </ol>

              <div>
                <h3><span>From the ground up</span></h3><span>In order to gain
                this information about the network all the bridges must
                exchange information and agree on the spanning tree. The
                exchange of information and method of consensus from the point
                of view of a bridge is as follows:</span>

                <ol>
                  <li><span>Upon starting up pick a random number as the ID and
                  assumes that it is the root of the tree and that no other
                  bridges exist. The root will be elected from among the
                  bridges by choosing the bridge with the lowest ID number. In
                  addition, each LAN will elect a designated bridge to handle
                  100% of the LANs traffic.</span></li>

                  <li>
                    <span>Broadcast a packet called a Bridge Protocol Data Unit
                    (BPDU) containing:</span>

                    <ul>
                      <li><span>the ID of the root bridge according to the
                      sending bridge</span></li>

                      <li><span>the cost of the path to the root bridge from
                      the sending bridge</span></li>

                      <li><span>the ID of the sending bridge</span></li>
                    </ul>
                  </li>

                  <li><span>Listen for BPDUs from neighbors</span></li>

                  <li><span>If a BPDU is read with a smaller root bridge ID,
                  record that ID as the new root, the port on which the BPDU
                  was received as the root port, and the cost to the root as
                  one greater than the cost reported by the BPDU to reflect the
                  path is one hop longer than the path for the bridge sending
                  the BPDU.</span></li>

                  <li><span>If a BPDU is read on a non-root port reporting a
                  better path to the root from the LAN on that port than
                  through the bridge, the bridge disables that port since a
                  better path exists for all of the traffic on that
                  LAN</span></li>

                  <li><span>Go to step 2. and repeat.</span></li>
                </ol>
              </div>

              <div>
                <h3>Complications</h3>The steps above are the essence of the
                Spanning Tree Protocol, but far from the only considerations
                when implementing the protocol on real bridges. The main
                confounding variable in the Spanning Tree Protocol is the
                dynamic nature of the network. At any time bridges (and hosts)
                can be added, moved within, or removed from the network, and
                the spanning tree found by the protocol may no longer be valid.
                Because of this it is necessary to extend the implementation of
                the protocol in certain ways. Bridges must repeatedly broadcast
                BPDUs to communicate that they are still active and their
                information is still valid. Any BPDU received should be
                considered valid only for very short period of time without
                receiving another, after which is should be timed out and the
                decisions about the network (root, root port, next hop, so on)
                made as a result of receiving the BPDU need to be reverted,
                since it is possible the bridge has fallen of the network.
                Because of the timeout property of BPDUs, each bridge must
                maintain a cache of BPDUs it has heard on each port, so that if
                one times out, the information about the network can be derived
                from BPDUs received which are still valid
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div><br>

  <footer class="page-footer blue-grey lighten-2">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Darren Roscoe</h5>

          <p class="grey-text text-lighten-4">View the code <a class=
          "grey-text text-lighten-4" href=
          "https://gitlab.com/darrenroscoe/bridge-stp"><u>here</u></a></p>
        </div>

        <div class="col l4 offset-l2 s12">
          <h5 class="white-text">Useful Resources</h5>

          <ul>
            <li><a class="grey-text text-lighten-3" href=
            "https://osrg.github.io/ryu-book/en/html/spanning_tree.html">Link
            1</a></li>

            <li><a class="grey-text text-lighten-3" href=
            "https://www.cisco.com/c/en/us/support/docs/lan-switching/spanning-tree-protocol/5234-5.html">
            Link 2</a></li>
          </ul>
        </div>
      </div>
    </div>

    <div class="footer-copyright">
      <div class="container"></div>
    </div>
  </footer><script type="text/javascript" src=
  "https://code.jquery.com/jquery-3.2.1.min.js">
</script><script src=
"https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"
  type="text/javascript">
</script>
</body>
</html>
