# Ethernet Bridge Spanning Tree Protocol

## Preface

Ethernet is a ubiquitous Internet communication medium, many consumers use Ethernet cables in their homes to connect their computer to the modem in their home, and the same technology is widely used across the Internet architecture to transfer data long distances. But Ethernet is far from perfect, and it has some limitations which require extra hardware to solve. We will begin this guide by describing Ethernet, some specific limitations, and why they necessitate bridges and the Spanning Tree Protocol (STP).

### Broadcast

Ethernet is a broadcast medium. Multiple hosts can be connected to the same wire (LAN), and any signal (packet) one host sends is visible to all hosts on the same LAN. Because of this only one host can be sending a packet at any given time or there will be interference and both packets will be corrupted, this is called a collision. On a LAN with relatively few hosts, this works well, but the more hosts on the LAN the more likely a collision is to occur, and throughput on the LAN drops as more hosts are added.

To mitigate this issue and allow more hosts on a network, the network must be broken up into collision domains, small sets of hosts whose packets may collide with each other but no other host's packets. To do this packets must be queued and forwarded between LANs. This is accomplished by the Ethernet Bridge.

### Distance

An Ethernet LAN has a maximum theoretical distance of about 2500m. Modern standard Ethernet Cables are usually only 100m. Thus to build a network spanning large distances requires a method of forwarding packets between LANs. Bridges serve this purpose.

### Why STP?

In the trivial case of one bridge connected to two LANs receiving a packet on one of them and forwarding it on the other, essentially a repeater, the implementation is simple, but with any more bridges and LANs involved, the implementation becomes more complex to account for confounding factors. One of these factors is packet forwarding. Bridges keep a record of which port packets from different hosts have been received on in order to forward packets to those hosts efficiently, and if the bridge is unaware of the location of a host then packets to that host are broadcast. The second and more significant factor is the possibility of loops in the network. A loop occurs when a packet follows a path that brings it to a bridge it has already been to, such as the following:

         (host A)
           /  \
          /    \
         /      \
        /        \
    (bridge 1)   (bridge 2)
         \        /
          \      /
           \    /
            \  /
          (host B)


Suppose in the network above, host A sends a packet to host B, both bridges receive the packet on the higher port, and forward it on the opposite port. Both bridges then receive the packet on the lower port and still don't know where host B is so they forward the packet on the higher port and the cycle continues. When this occurs the packet can cycle indefinitely bringing the network down, so bridges need to be able to avoid loops. The way this is accomplished is by constructing a spanning tree over the graph of the network and only forwarding packets within the tree. The Bridge Spanning Tree Protocol is used to construct this spanning tree without any bridge needing to know the full layout of the network.


## STP

The goal of STP is to build a spanning tree in the network. This is accomplished by each bridge disabling a subset of its ports such that no cycles exist in the network graph. In order to know which ports to disable the bridges communicate with their direct neighbors to pass information about the state of the network across the network so each bridge can enable or disable its ports accordingly. The information a bridge needs about the network is as follows:

* Which bridge is at the root of the tree
* Which port leads toward the root with minimal cost (in hops)
* The cost of the path to the root for bridges on LANs connected to ports on the bridge

### From the ground up

In order to gain this information about the network all the bridges must exchange information and agree on the spanning tree, since multiple spanning trees can be built from the same network. The exchange of information and method of consensus is as follows:

1. Each bridge on starting up picks a random number as its ID and assumes that it is the root of the tree and that no other bridges exist. The root will be elected from among the bridges by choosing the bridge with the lowest ID number.
2. Each bridge broadcasts a packet called a Bridge Protocol Data Unit (BPDU) containing: the ID of the root bridge according to the sending bridge, the cost of the path to the root bridge from the sending bridge, and the ID of the sending bridge
3. Each bridge receives BPDU from its neighbors
4. If a BPDU is read with a smaller root bridge ID, the bridge records that ID as the new root, the port on which the BPDU was received as the root port, and the cost to the root as one greater than the cost reported by the BPDU to reflect the path is one hop longer than the path for the bridge sending the BPDU.
5. In addition each group of bridges on a LAN elects a designated bridge which all pakcets from the LAN will be forwarded by. This is chosen for the lowest cost path to the root, or by lowest bridge ID in the case of a tie.
6. If a bridge receives a BPDU on a port that is not the root port reporting the same root port that the bridge knows of and a cost which is one less than that of the bridge, or the same as that of the bridge but with a lower bridge ID, the bridge will disable that port (will not forward packets, but will still listen for BPDUs) since a better path to the root from that lan exists.
7. Go to step 2. and repeat.


### Complications

The steps above are the essence of the Spanning Tree Protocol, but far from the only considerations when implenting the protocol on real bridges. The main confounding variable in the Spanning Tree Protocol is the dynamic nature of the network. At any time bridges (and hosts) can be added, moved within, or removed from the network, and the spanning tree found by the protocol may no longer be valid. Because of this it is necessary to extend the implementation of the protocol.

* Bridges must repeatedly broadcast BPDUs to communicate that they are still active and their information is still valid.
* Any BPDU received should be considered valid only for very short period of time without receiving another, after which is should be timed out and the decisions about the network (root, root port, next hop, so on) made as a result of receiving the BPDU need to be reverted, since it is possible the bridge has fallen of the network.
* Because of the timeout property of BPDUs, each bridge must maintain a cache of BPDUs it has heard on each port, so that if one times out, the information about the network can be derived from BPDUs received which are still valid


## Packet forwarding

Once the network converges on a spanning tree solution, each bridge can forward packets correctly simply by broadcasting received packets on all ports which are not disabled and not the port on which the packet was received, and no loops will occur. To minimize packet redundancy bridges also learn where to forward packets to certain hosts by listening to packets passing through and recording the receiving port and the sending host to record that the sending host can be reached via the receiving port. This data also needs to timeout after a certain time in case a host moves to a different place in the network.
